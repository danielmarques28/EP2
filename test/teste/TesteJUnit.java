/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teste;

import comida.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import tocomfome.Pedido;

/**
 *
 * @author daniel
 */
public class TesteJUnit {
    
    public TesteJUnit() {
        
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testarProdutos(){
        Prato prato = new Prato();
        prato.setNome("Hamburguer");
        prato.setPreco(9.90f);
        prato.setQuantidade(30);
        Assert.assertEquals("Hamburguer", prato.getNome());
        Assert.assertEquals(9.90f, prato.getPreco(),0);
        Assert.assertEquals(30, prato.getQuantidade());
      
    }
    
    @Test
    public void testarPedido(){
        Pedido pedidos = new Pedido();
        pedidos.setBebidaNome("Sprite");
        pedidos.setBebidaQtde(4);
        pedidos.setData("20/10/2016");
        pedidos.setHora("20:10:16");
        pedidos.setValorTotal(45.44f);
        Assert.assertEquals("Sprite", pedidos.getBebidaNome());
        Assert.assertEquals(4, pedidos.getBebidaQtde());
        Assert.assertEquals("20/10/2016", pedidos.getData());
        Assert.assertEquals("20:10:16", pedidos.getHora());
        Assert.assertEquals(45.44f, pedidos.getValorTotal(), 0);
        
    }
    
    @Test
    public void testarEstoque() {
        Estoque estoque = new Estoque();
        Bebida bebida = new Bebida();
        bebida.setNome("Coca-Cola");
        bebida.setPreco(3.55f);
        bebida.setQuantidade(2);
        estoque.adicionarBebida(bebida);
        Assert.assertEquals("Coca-Cola", estoque.getBebidaNome(0));
    }
}

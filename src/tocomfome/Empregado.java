package tocomfome;

public class Empregado {
	public String usuario;
	public String senha;
	
	public Empregado() {
		usuario = "";
		senha = "";

	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
}

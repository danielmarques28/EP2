package tocomfome;

import java.text.SimpleDateFormat;
import java.util.Date;

import pagamento.*;
import comida.*;

public class Pedido extends Cliente{
	private float valorTotal;
        private String observacao;
	private String data;
	private String hora;
        private Bebida bebida;
        private Prato prato;
        private Sobremesa sobremesa;
        private Cartao cartao;
        private Dinheiro dinheiro;
	
	public Pedido(){
            this.bebida = new Bebida();
            this.prato = new Prato();
            this.sobremesa = new Sobremesa();
            this.cartao = new Cartao();
            this.dinheiro = new Dinheiro();
            
            Date date = new Date();
            SimpleDateFormat horaSDF = new SimpleDateFormat("HH:mm:ss");
            SimpleDateFormat dateSDF = new SimpleDateFormat("dd/MM/yyyy");
            setHora(horaSDF.format(date));
            setData(dateSDF.format(date));
		
	}

        public float getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal(float valorTotal) {
		this.valorTotal = valorTotal;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
        
        public void setBebidaNome(String nomeBebida){
            this.bebida.setNome(nomeBebida);
        }
        
        public String getBebidaNome(){
            return bebida.getNome();
        }
        
        public void setBebidaQtde(int quantidade){
            this.bebida.setQuantidade(quantidade);
        }
        
        public int getBebidaQtde(){
            return bebida.getQuantidade();
        }
        
        public void setPratoNome(String nomePrato){
            this.prato.setNome(nomePrato);
        }
        
        public String getPratoNome(){
            return prato.getNome();
        }
        
        public void setPratoQtde(int quantidade){
            this.prato.setQuantidade(quantidade);
        }
        
        public int getPratoQtde(){
            return prato.getQuantidade();
        }
        
        public void setSobremesaNome(String nomeSobremesa){
            this.sobremesa.setNome(nomeSobremesa);
        }
        
        public String getSobremesaNome() {
            return sobremesa.getNome();
        }
        
        public void setSobremesaQtde(int quantidade){
            this.sobremesa.setQuantidade(quantidade);
        }
        
        public int getSobremesaQtde(){
            return sobremesa.getQuantidade();
        }
        
        public void setCartao(boolean uso){
            this.cartao.setPagamento(uso);
        }
        
        public boolean getCartao(){
            return cartao.getPagamento();
        }
        
        public void setDinheiro(boolean uso){
            this.dinheiro.setPagamento(uso);
        }
        
        public boolean getDinheiro(){
            return dinheiro.getPagamento();
        }
        
}

package comida;

import java.util.ArrayList;

public class Estoque {
    private ArrayList<Bebida> listaBebida;
    private ArrayList<Prato> listaPrato;
    private ArrayList<Sobremesa> listaSobremesa;
    
    public Estoque() {
        listaBebida = new ArrayList<>();
        listaPrato = new ArrayList<>();
        listaSobremesa = new ArrayList<>();
    }

    public void adicionarPrato(Prato prato){
        this.listaPrato.add(prato);
    }
    
    public void adicionarBebida(Bebida bebida){
        this.listaBebida.add(bebida);
    }
    
    public void adicionarSobremesa(Sobremesa sobremesa){
        this.listaSobremesa.add(sobremesa);
    }
    
    public void removerPrato(Prato prato){
        this.listaPrato.remove(prato);
    }
    
    public void removerBebida(Bebida bebida){
        this.listaBebida.remove(bebida);
    }
    
    public void removerSobremesa(Sobremesa sobremesa){
        this.listaSobremesa.remove(sobremesa);
    }
    
    public String getPratoNome(int posicao){
        return listaPrato.get(posicao).getNome();
    }
    
    public String getBebidaNome(int posicao){
        return listaBebida.get(posicao).getNome();
    }
    
    public String getSobremesaNome(int posicao){
        return listaSobremesa.get(posicao).getNome();
    }
    
    public float getPratoPreco(int posicao){
        return listaPrato.get(posicao).getPreco();
    }
    
    public float getBebidaPreco(int posicao){
        return listaBebida.get(posicao).getPreco();
    }
    
    public float getSobremesaPreco(int posicao){
        return listaSobremesa.get(posicao).getPreco();
    }
    
    public int getPratoQtde(int posicao){
        return listaPrato.get(posicao).getQuantidade();
    }
    
    public int getBebidaQtde(int posicao){
        return listaBebida.get(posicao).getQuantidade();
    }
    
    public int getSobremesaQtde(int posicao){
        return listaSobremesa.get(posicao).getQuantidade();
    }
   
    public int tamPrato(){
        return listaPrato.size();
    }
    
    public int tamBebida(){
        return listaBebida.size();
    }
    
    public int tamSobremesa(){
        return listaSobremesa.size();
    }
    
    public void mudarBebida(int posicao, int qtdeNova){
        this.listaBebida.get(posicao).setQuantidade(qtdeNova);
    }
    
    public void mudarPrato(int posicao, int qtdeNova){
        this.listaPrato.get(posicao).setQuantidade(qtdeNova);
    }
    
    public void mudarSobremesa(int posicao, int qtdeNova){
        this.listaSobremesa.get(posicao).setQuantidade(qtdeNova);
    }
    
}

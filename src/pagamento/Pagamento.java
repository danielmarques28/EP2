package pagamento;

abstract class Pagamento {
	protected boolean pagamento;

	public boolean getPagamento() {
		return pagamento;
	}
	public void setPagamento(boolean pagamento) {
		this.pagamento = pagamento;
	}
	
}
